# use this image
FROM node:10

# copy from, to
COPY ["package.json", "package-lock.json", "/usr/src/"]

# cd directory
WORKDIR /usr/src

# run command
RUN npm install

# copy from, to
COPY [".", "/usr/src/"]

# Publish port
EXPOSE 3000

# Default container command to run on boot
CMD ["node", "index.js"]
